#ifndef ALUNO_HPP
#define ALUNO_HPP

#include <iostream>
#include <string>
#include "pessoa.hpp"

using namespace std;

class aluno: public Pessoa {
private:
// Atributos
  string curso;
  float ira;
// Métodos
public:
  aluno();
  ~aluno();
  float getIra();
  string getCurso();
  void setCurso(string curso);

};


#endif






