#ifndef SERVIDOR_HPP
#define SERVIDOR_HPP

#include <iostream>
#include <string>
#include "pessoa.hpp"

using namespace std;

class servidor: public Pessoa {
private:
// Atributos
  string cargo;
  string departamento;
  int carga_horaria;
// Métodos
public:
  servidor();
  ~servidor();
  string getcargo();
  void setCargo(string cargo);
  string getDepartamento();
  void setDepartamento(string departamento);
  void setIndiceDeAprovacao(float indice);
  int getCargaHoraria();
  void setCargaHoraria(int carga_horaria);
};


#endif






