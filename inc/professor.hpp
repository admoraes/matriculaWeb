#ifndef PROFESSOR_HPP
#define PROFESSOR_HPP

#include <iostream>
#include <string>
#include "pessoa.hpp"

using namespace std;

class professor: public Pessoa {
private:
// Atributos
  string formacao;
  string departamento;
  float indice_de_aprovacao;
// Métodos
public:
  professor();
  ~professor();
  string getFormacao();
  void setFormacao(string formacao);
  string getDepartamento();
  void setDepartamento(string departamento);
  void setIndiceDeAprovacao(float indice);
  float getIndiceDeAprovacao();

};


#endif






