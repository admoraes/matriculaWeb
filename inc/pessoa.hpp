#ifndef PESSOA_HPP
#define PESSOA_HPP

#include <iostream>
#include <string>

using namespace std;

class Pessoa {
private:
// Atributos
        int matricula;
	string nome;
        long int cpf;
	string telefone;
	int idade;
// Métodos
public:
	Pessoa();	// Construtor
  Pessoa(int matricula, string nome, long int cpf, string telefone, int idade);
	~Pessoa();	// Destrutor
	void setNome(string nome);
	string getNome();
	void setTelefone(string telefone);
	string getTelefone();
	void setIdade(int idade);
	int getIdade();
	void incrementaIdade();

};


#endif






